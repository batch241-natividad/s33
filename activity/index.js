fetch('https://jsonplaceholder.typicode.com/todos')

.then(response => response.json())
.then(json => {
	const titles = json.map(item => item.title);
	console.log(titles);
})

fetch('https://jsonplaceholder.typicode.com/todos/40')
.then(response => response.json())
.then(json => {
	console.log(`Title: ${json.title}, Status: ${json.completed}`);
})

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created to do List item',
		complete: false
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'delectus aut autem',
    completed: false
  })
})
  .then(response => response.json())
  .then(json => console.log(json))

  fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'delectus aut autem',
    description: 'To update my to do list with a different data structure',
    status: 'Pending',
    userID: 1
  })
})
  .then(response => response.json())
  .then(json => console.log(json))

  fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
   title: 'My updated to-do item',
    description: 'This is the updated description',
    status: 'completed',
    dateCompleted: '07/09/21',
    userID: 1
  })
})
  .then(response => response.json())
  .then(json => console.log(json))


  fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});